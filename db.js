'use strict'

let mysql = require('promise-mysql')
require('dotenv').load()

let connection = null

function connect() {
  return mysql.createConnection({
    host: process.env.DB_HOST,
    user: process.env.DB_USER,
    password: process.env.DB_PASS,
    database: process.env.DB_NAME,
  }).then((_connection) => {
    connection = _connection
    return Promise.resolve(connection)
  }).catch(console.error)
}

function getConnection() {
  if (connection != null) {
    return Promise.resolve(connection)
  }
  return connect()
}

module.exports = {
  getConnection: getConnection
}
