'use strict'

let db = require('./db')

class ReservoirResource {
  constructor() {
    this._connect()
  }

  _connect() {
    db.getConnection()
      .then((connection) => {
        this.connection = connection
      }).catch(console.error)
  }

  queryStorage(stationId) {
    return this.connection.query(`
      SELECT Amount, Timestamp
      FROM Reservoir_Storage
      JOIN Reservoir ON Reservoir_Storage.Reservoir_ID=Reservoir.Reservoir_ID
      WHERE Reservoir.Station_ID = 'SHA'
      ORDER BY Timestamp DESC
      LIMIT 1
      `)
    }
}

module.exports = ReservoirResource
