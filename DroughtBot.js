'use strict'

var Botkit = require('botkit')

class DroughtBot {
  constructor(token, reservoirResource) {
    this.reservoirResource = reservoirResource
    this._connect(token)
    this._listen()
  }

  _connect(token) {
    this.controller = Botkit.slackbot()
    this.controller.spawn({token}).startRTM()
  }

  _listen() {
    this.controller.on('direct_mention', (bot, message) => {
      let reservoirInquiryMatch = message.text.match(/([A-Z]{3}) reservoir/)

      if (reservoirInquiryMatch) {
        let stationId = reservoirInquiryMatch[1]
        this.reservoirResource.queryStorage(stationId).then(storage => {
          console.log(storage[0]);
          if (storage) {
            bot.reply(message,`${stationId} was at ${storage[0].Amount} acre-feet as of ${storage[0].Timestamp}`)
          } else {
            bot.reply(message, `I don't know reservoir ${stationId}` )
          }
        })
      } else {
        bot.reply(
          message,
          'You can ask me about "XXX reservoir" (with real three-letter code)'
        )
      }
    })
  }
}

module.exports = DroughtBot
